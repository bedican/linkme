#! /usr/bin/env node

const nodeGetopt = require('node-getopt');
const linkme = require('../lib/linkme');

var help =
    "\nUsage:" +
    "\n  linkme [dependency]" +
    "\n\nOptions:" +
    "\n[[OPTIONS]]\n";

var opts = [
    ['u', 'unlink', 'Unlink'],
    ['h', 'help', 'Displays this help']
];

var getopt = nodeGetopt.create(opts).setHelp(help).bindHelp();
var opts = getopt.parseSystem();

function usage(message) {
    if (message) {
        console.info(message);
    }

    getopt.showHelp();
    process.exit();
}

try {
    linkme.run(opts.argv, opts.options);
} catch(e) {
    usage(e.message);
}
