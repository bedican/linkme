# Linkme

A simple utility to create symbolic links to locally checked out dependencies.

Inspired by the functionality provided by `npm link` but for non npm or node managed projects where similar functionality is missing.

Currently only supports [composer](https://getcomposer.org/) linking.

Linkme works in two parts, local and global linking/unlinking.

## Global linking

A link is created from a global location `<home dir>/.linkme/<dependency manager>/<cononical package name>` to your project.

This creates a way of linking in to a locally checked out dependency.

## Local linking

A link is created from the dependency managers directory (e.g. `vendor` for composer) to the global link of the specified dependency.

## Installation

``` bash
# Install the module globally
$ sudo npm -g install linkme
```

## Usage

###

``` bash
# cd to your dependency
$ cd /path/to/my-dependency

# Create a global link
$ linkme

# or remove the global link
$ linkme -u

# cd to your application where we wish to link to the dependency.
$ cd /path/to/my-app

# Create a link to your dependency within the application
$ linkme my-dependency

# or unlink the dependency from the application
$ linkme -u my-dependency
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
