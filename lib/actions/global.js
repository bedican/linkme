const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');

module.exports = {
    link: function(config, pm) {

        if (!pm.inPackage()) {
            console.error('Could not create global link, are you in the right directory?');
            return;
        }

        var target = process.cwd();
        var symlink = [
            config.getGlobalDir(),
            pm.getName(),
            pm.safePackageName(pm.getCurrentPackage())
        ].join(path.sep);

        try {
            var stats = fs.lstatSync(symlink);
            if ((stats.isFile()) || (stats.isSymbolicLink())) {
                fs.unlinkSync(symlink);
            } else if(stats.isDirectory(symlink)) {
                console.error("'" + symlink + "' exists and is a directory");
                return;
            }
        } catch(e) {
            // Silently continue
        }

        mkdirp.sync(path.dirname(symlink));
        fs.symlinkSync(target, symlink);

        console.info("'" + symlink + "' -> '" + target + "'");
    },
    unlink: function(config, pm) {

        if (!pm.inPackage()) {
            console.error('Could not remove global link, are you in the right directory?');
            return;
        }

        var symlink = [
            config.getGlobalDir(),
            pm.getName(),
            pm.safePackageName(pm.getCurrentPackage())
        ].join(path.sep);

        try {
            var stats = fs.lstatSync(symlink);
            if ((stats.isFile()) || (stats.isSymbolicLink())) {
                fs.unlinkSync(symlink);
            } else if(stats.isDirectory(symlink)) {
                console.error("'" + symlink + "' is a directory and was not removed");
                return;
            }
        } catch(e) {
            console.error(e.message);
            return;
        }

        console.info("Removed '" + symlink + "'");
    }
};
