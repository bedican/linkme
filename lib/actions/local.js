const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const rimraf = require('rimraf');

module.exports = {
    link: function(pkg, config, pm) {

        if (!pm.inPackage()) {
            console.error('Could not create local link, are you in the right directory?');
            return;
        }

        var target = [
            config.getGlobalDir(),
            pm.getName(),
            pm.safePackageName(pkg)
        ].join(path.sep);

        var symlink = [
            pm.getDepsDir(),
            pm.packageNameToDir(pkg)
        ].join(path.sep);

        try {
            var stats = fs.lstatSync(symlink);
            if ((stats.isFile()) || (stats.isSymbolicLink())) {
                fs.unlinkSync(symlink);
            } else if(stats.isDirectory(symlink)) {
                rimraf.sync(symlink);
                //fs.rmdirSync(symlink);
            }
        } catch(e) {
            // Silently continue
        }

        mkdirp.sync(path.dirname(symlink));
        fs.symlinkSync(target, symlink);

        console.info("'" + symlink + "' -> '" + target + "'");
    },
    unlink: function(pkg, config, pm) {

        if (!pm.inPackage()) {
            console.error('Could not remove local link, are you in the right directory?');
            return;
        }

        var symlink = [
            pm.getDepsDir(),
            pm.packageNameToDir(pkg)
        ].join(path.sep);

        try {
            var stats = fs.lstatSync(symlink);
            if ((stats.isFile()) || (stats.isSymbolicLink())) {
                fs.unlinkSync(symlink);
            } else if(stats.isDirectory(symlink)) {
                console.error("'" + symlink + "' is a directory and was not removed");
                return;
            }
        } catch(e) {
            console.error(e.message);
            return;
        }

        console.info("Removed '" + symlink + "'");
    }
};
