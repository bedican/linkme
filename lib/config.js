const os = require('os');
const path = require('path');

module.exports = {
    getGlobalDir: function() {
        return [
            os.homedir(),
            '.linkme'
        ].join(path.sep);
    }
};
