const packages = require('./packages/packages');
const actions = require('./actions/actions');
const config = require('./config');

module.exports = {
    run: function(args, options) {

        var pm = packages.composer; // TODO, add more and method on selecting which

        var pkg = args.shift();
        var action = options.unlink ? 'unlink' : 'link';

        if (pkg) {
            actions.local[action](pkg, config, pm);
            return;
        }

        actions.global[action](config, pm);
    }
};
