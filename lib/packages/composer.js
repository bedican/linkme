const fs = require('fs');
const path = require('path');

const JSONFILE = process.cwd() + path.sep + 'composer.json';

var composerJson = false;

module.exports = {
    inPackage: function() {
        return fs.existsSync(JSONFILE);
    },
    getName: function() {
        return 'composer';
    },
    getDepsDir: function() {
        return 'vendor';
    },
    getCurrentPackage: function() {
        if (!composerJson) {
            composerJson = JSON.parse(fs.readFileSync(JSONFILE, 'utf8'));
        }

        return composerJson.name;
    },
    packageNameToDir: function(pkg) {
        return pkg.replace('/', path.sep);
    },
    safePackageName: function(pkg) {
        return pkg.replace(path.sep, '_');
    }
};
